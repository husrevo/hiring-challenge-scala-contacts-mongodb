import com.vngrs.contacts.RawContact

/**
 * Created by husrevo on 27/10/15.
 */
trait SampleContacts {
  val husrev = RawContact("husrevo", "Hüsrev", "Özayman", "+90 555 629 8980")
  val husrev2 = RawContact("husrevo2", "hüsrev", "özayman", "+90 212 603 7677")
  val husrev3 = RawContact("husrevo3", "Husrev", "Ozayman", "0 212 603 7677")
  val musa =  RawContact("musakarakas", "Musa", "Karakaş", "+90 535 315 7003")
  val musa2 =  RawContact("musakarakas2", "Musa", "Karakaş", "+905353157003")
  val vngrs = RawContact("VNGRS", "VNGRS", "Ltd.", "+90 212 328 0290 ")
  val husrevs = List(husrev, husrev2, husrev3)
  val musas = List(musa, musa2)
  val contacts = vngrs :: husrevs ++ musas

  val extraMiddleName = RawContact("middle", "first middle", "last", "1-800-call-me-maybe")
}
