import java.io.{File, FileNotFoundException}

import com.vngrs.contacts._
import org.scalatest._
import scala.util.{Failure, Success}
import scala.xml.XML

class ContactsMergerSpec extends FlatSpec with Matchers with SampleContacts {

  "ContactsMerger" should "merge contacts with exactly same fullnames" in {
    val mergedContacts = ContactsMerger.mergeRawContacts(contacts, Filters.identity)
    val maybeMusa = mergedContacts.find(_.ids.contains(musa.id))
    checkIfMerged(maybeMusa, musas)
  }

  it should "merge contacts with same fullnames when lowered and ascii folded" in {
    val mergedContacts = ContactsMerger.mergeRawContacts(contacts)
    val maybeHusrev = mergedContacts.find(_.ids.contains(husrev.id))
    checkIfMerged(maybeHusrev, husrevs)
  }

  it should "not merge irrelevant contacts " in {
    val mergedContacts = ContactsMerger.mergeRawContacts(contacts)
    val maybeVngrs = mergedContacts.find(_.ids.contains(vngrs.id))
    maybeVngrs should be ('isDefined)
    val Some(mergedVngrs) = maybeVngrs
    mergedVngrs shouldEqual MergedContact(vngrs.id, vngrs.name, vngrs.lastName, Set(vngrs.phone), Set(vngrs.id))
  }

  private def checkIfMerged(maybeMerged: Option[MergedContact], rawContacts: List[RawContact]): Unit = {
    maybeMerged should be ('isDefined)
    val Some(mergedContact) = maybeMerged
    rawContacts.map(_.id) should contain(mergedContact.id)
    mergedContact.phones shouldEqual rawContacts.map(_.phone).toSet
    mergedContact.ids shouldEqual rawContacts.map(_.id).toSet
  }
}
