import com.typesafe.config.ConfigFactory
import com.vngrs.contacts._
import org.scalatest._
import reactivemongo.api.commands.UpdateWriteResult
import scala.concurrent.duration._

import scala.concurrent.{Future, Await}

class MongoContactsSpec extends FlatSpec with Matchers with SampleContacts {
  val testConfig = ConfigFactory.load().getConfig("test")
  val mongoContacts = new MongoContacts(testConfig)

  "MongoContacts" should "insert a single contact" in {
    val result = mergeAndSave(List(husrev), wipe = true)
    result.ok shouldBe true
    result.n shouldEqual 1
  }

  it should "find only that contact back" in {
    val result = waiting(mongoContacts.findByName("Hüsrev", true))
    result should have length 1
    result shouldEqual ContactsMerger.mergeRawContacts(List(husrev))
  }

  it should "merge second contact when called with wipe = false" in {
    val result = mergeAndSave(List(husrev2, husrev3), wipe = false)
    result.ok shouldBe true
    result.upserted should have length 0
    result.nModified shouldBe 1
    val contacts = waiting(mongoContacts.findByName("hüsrev", true))
    contacts should have length 1
    contacts.head.phones should have size 3
    contacts shouldEqual ContactsMerger.mergeRawContacts(husrevs)
  }

  it should "be able to find someone by their middle name with startsWith = false" in {
    val result = mergeAndSave(List(vngrs, extraMiddleName), wipe = true)
    val contacts = waiting(mongoContacts.findByName("middle", startsWith = false))
    contacts should have length 1
    contacts shouldEqual ContactsMerger.mergeRawContacts(List(extraMiddleName))
  }

  def mergeAndSave(contacts: List[RawContact], wipe: Boolean): UpdateWriteResult = {
    waiting {
      mongoContacts.saveToDB(ContactsMerger.mergeRawContacts(contacts), wipe)
    }
  }

  def waiting[T](f: Future[T]) = {
    Await.result(f, 10 seconds)
  }
}
