import java.io.{FileNotFoundException, File}

import com.vngrs.contacts.{ContactsParser, RawContact}
import org.scalatest._

import scala.util.{Try, Failure, Success}
import scala.xml.XML

class ContactsParserSpec extends FlatSpec with Matchers with SampleContacts {
  "ContactsParser" should "be able to parse a single XML contact" in {
    val subjectOfTest = husrev
    val parsedContacts = ContactsParser.rawContactsFromXML(createXmlFromRawContact(subjectOfTest))
    subjectOfTest shouldEqual parsedContacts(0)
  }

  it should "be able to parse multiple XML contacts" in {
    val xml =
      <contacts>
        {contacts map createXmlFromRawContact}
      </contacts>
    contacts shouldEqual ContactsParser.rawContactsFromXML(xml)
  }

  it should "skip unknown elements and elements with missing fields" in {
    val xml =
      <contacts>
        <contact>
          <id>without-any-name</id>
          <phones>+90 212 444 4 444</phones>
        </contact>
        {contacts map createXmlFromRawContact}
        <apple>
          <forces>vendor lock-in</forces>
        </apple>
      </contacts>

    contacts shouldEqual ContactsParser.rawContactsFromXML(xml)
  }

  it should "be able to parse from a file" in {
    val tmpFile = File.createTempFile("contacts", ".xml")
    val xml =
      <contacts>
        {contacts map createXmlFromRawContact}
      </contacts>
    XML.save(tmpFile.getAbsolutePath, xml, "UTF-8")

    Success(contacts) shouldEqual ContactsParser.rawContactsFromFile(tmpFile)
  }

  it should "return a FileNotFoundException when given a non-existing file" in {
    val tmpFile = File.createTempFile("contacts", ".xml")
    tmpFile.delete();

    ContactsParser.rawContactsFromFile(tmpFile) should matchPattern {
      case Failure(e : FileNotFoundException) =>
    }
  }


  def createXmlFromRawContact(rawContact: RawContact) = {
    <contact>
      <id>{rawContact.id}</id>
      <name>{rawContact.name}</name>
      <lastName>{rawContact.lastName}</lastName>
      <phones>{rawContact.phone}</phones>
    </contact>
  }
}
