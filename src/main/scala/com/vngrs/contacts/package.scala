package com.vngrs

import java.util.Locale

import com.vngrs.contacts.Filters.StringFilter
import reactivemongo.bson.{BSONDocumentReader, BSONDocument, BSONDocumentWriter}

/**
 * Created by husrevo on 25/10/15.
 */
package object contacts {
  sealed trait NamedContact {
    def name: String
    def lastName: String
    def fullNameHash(filter : StringFilter = Filters.defaultFilter) = (filter(name), filter(lastName)).hashCode
  }
  case class RawContact(id: String, name: String, lastName:String, phone: String) extends NamedContact
  case class MergedContact(id: String, name: String, lastName:String, phones: Set[String], ids:Set[String]) extends NamedContact


  object MergedContact {
    implicit val contactWriter = new BSONDocumentWriter[MergedContact] {
      override def write(contact: MergedContact): BSONDocument =
        BSONDocument(
          "_id" -> contact.fullNameHash(),
          "id" -> contact.id,
          "name" -> contact.name,
          "lastName" -> contact.lastName,
          "phones" -> contact.phones,
          "ids" -> contact.ids,
          "name_clean" -> Filters.defaultFilter(contact.name),
          "name_search" -> contact.name
        )
    }
    implicit val contactReader = new BSONDocumentReader[MergedContact] {
      override def read(bson: BSONDocument): MergedContact = {
        val maybeContact =
          for(
            id <- bson.getAsTry[String]("id");
            name <- bson.getAsTry[String]("name");
            lastName <- bson.getAsTry[String]("lastName");
            phones <- bson.getAsTry[Set[String]]("phones");
            ids <- bson.getAsTry[Set[String]]("ids")
          ) yield MergedContact(id,name,lastName,phones,ids)

        maybeContact.get
      }
    }
  }

  object Filters {
    type StringFilter = Filter[String]
    type Filter[T] = (T) => T

    val identity = (s : String) => s
    val turkishLowerCaseFilter = (s: String) => s.toLowerCase(Locale.forLanguageTag("tr"))
    val defaultLocaleLowerCaseFilter = (s: String) => s.toLowerCase
    val replaceTurkishChars = (s: String) => s map {
      case 'ü' => 'u'
      case 'ö' => 'o'
      case 'ğ' => 'g'
      case 'ş' => 's'
      case 'ç' => 'c'
      case 'ı' => 'i'
      case c => c
    }

    val defaultFilter = turkishLowerCaseFilter andThen replaceTurkishChars
  }
}
