package com.vngrs.contacts


import com.vngrs.contacts.Filters.StringFilter

/**
 * Created by husrevo on 25/10/15.
 */
object ContactsMerger {
  def mergeRawContacts(rawContacts: List[RawContact], filter: StringFilter = Filters.defaultFilter) : List[MergedContact] = {
    val groups = rawContacts.groupBy(_.fullNameHash(filter))
    val mergedContacts = for((_, group) <- groups) yield {
      val head = group.head
      MergedContact(head.id, head.name, head.lastName, group.map(_.phone).toSet, group.map(_.id).toSet)
    }
    mergedContacts.toList
  }
}
