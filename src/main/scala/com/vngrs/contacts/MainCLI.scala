package com.vngrs.contacts

import java.io.File

import scala.concurrent.Await
import scala.util.{Success, Failure}
import scala.concurrent.duration._


/**
 * Created by husrevo on 25/10/15.
 */

sealed trait Command
case class Migrate(files: Seq[File] = Seq.empty, wipe: Boolean = false) extends Command
case class Query(name: String = "", startsWith: Boolean = false, max:Int = 100) extends Command
case class Options(command: Command = Query(), mongoUrl: Option[String] = None)

object MainCLI extends App {
  val parser = new scopt.OptionParser[Options]("java -jar contacts.jar") {
    head("contacts", "0.9.x")
    help("help") text("prints this help message\n")
    opt[String]("mongoConn") abbr("mc") action { (x, c) =>
      c.copy(mongoUrl = Some(x))
    } text("mongo connection string to override db conf. collection name still will be contacts\n")
    cmd("import") action { (_, c) =>
      c.copy(command = Migrate()) } text("use import to parse and import your contacts to MongoDB") children(
      arg[File]("<file>...") unbounded() optional() action { (x, c) =>
        val m = c.command.asInstanceOf[Migrate]
        c.copy(command = m.copy(files = m.files :+ x))
      } text("files to be imported"),
      opt[Unit]('w', "wipe") action { (_, c) =>
        val m = c.command.asInstanceOf[Migrate]
        c.copy(command = m.copy(wipe = true))
      } text("wipe the db first."),
      checkConfig { c =>
        c match {
          case Options(m: Migrate, _) =>
            if (m.files.size == 0) failure("import needs a file") else success
          case _ => success
        }
      }
      )
    cmd("search") action { (_, c) =>
      c.copy(command = Query()) } text("use search to search contacts by name") children(
      arg[String]("<name>") action { (x, c) =>
        val q = c.command.asInstanceOf[Query]
        c.copy(q.copy(name = x))
      } text("name to be searched, whole name if not used with --starts-with (-s) switch"),
      opt[Unit]('s', "starts-with") action { (x, c) =>
        val q = c.command.asInstanceOf[Query]
        c.copy(q.copy(startsWith = true))
      } text("to fing records with names starting with your query"),
      opt[Int]('n', "max") action { (x, c) =>
        val q = c.command.asInstanceOf[Query]
        c.copy(q.copy(max = x))
      } text("maximum number of results, defaults to 100")
      )
    }
  parser.parse(args, Options()) match {
    // match default case to show help istead.
    case Some(Options(Query("", false, 100), None)) => println("run --help to see help")
    case Some(options) =>
      val mongoUrl = options.mongoUrl
      val mongoContacts = new MongoContacts(mongoUri = mongoUrl);
      options.command match {
        case Migrate(files, wipe) =>
          migrate(mongoContacts, files, wipe)
        case Query(subject, startsWith, max) =>
          search(mongoContacts, subject, startsWith, max)
      }
      mongoContacts.close()
    case None =>
    // arguments are bad, error message will have been displayed
  }

  def migrate(mongoContacts: MongoContacts, files: Seq[File], wipe: Boolean): Unit = {
    val raws = files.map(ContactsParser.rawContactsFromFile)
    val faileds = raws.zip(files).filter(_._1.isFailure)
    if (faileds.size > 0) {
      for ((Failure(err), file) <- faileds) {
        println(file.getAbsolutePath + " failed:")
        err.printStackTrace()
        println("\n\n")
      }
    } else {
      val all = raws.collect { case Success(list) => list }.flatten
      println(s"read ${all.size} records")
      val merged = ContactsMerger.mergeRawContacts(all.toList)
      println(s"merged into ${merged.size} contacts")
      val futureOfResult = mongoContacts.saveToDB(merged, wipe)
      val result = Await.result(futureOfResult, 5 minutes)
      val newContacts = if (wipe) result.n else result.upserted.size
      println(s"""
           | ok: ${result.ok}
           | inserted ${newContacts} new contacts
           | updated ${result.nModified} contacts
           | err: ${result.errmsg.getOrElse("NONE")}
           | writeErrors:
           |     ${result.writeErrors.map(_.toString).mkString("\n    ")}
           |""".stripMargin)
    }
  }

  def search(mongoContacts: MongoContacts, subject: String, startsWith: Boolean, max: Int): Unit = {
    val futureOfContacts = mongoContacts.findByName(subject, startsWith, max)
    val contacts = Await.result(futureOfContacts, 5 minutes)
    contacts.zipWithIndex foreach { case (contact,i) =>
      println(s"""
         | #${i+1}
         | name: ${contact.name}
         | lastName: ${contact.lastName}
         | phones:
         |   ${contact.phones.mkString("\n   ")}
         |
       """.stripMargin)
    }
  }
}
