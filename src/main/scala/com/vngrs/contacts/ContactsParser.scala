package com.vngrs.contacts

import java.io.File

import scala.util.{Failure, Success, Try}
import scala.xml.{XML, Node, Elem}

/**
 * Created by husrevo on 25/10/15.
 */
object ContactsParser {
  def rawContactsFromFile(file: File) : Try[List[RawContact]] = {
    try {
      val xml = XML.loadFile(file)
      Success(rawContactsFromXML(xml))
    } catch {
      case e: Exception => Failure(e)
    }
  }

  def rawContactsFromXML(elem: Elem) : List[RawContact] = {
    (elem \\ "contact").map(elemToRawContact).collect{ case Some(contact) => contact}.toList
  }

  private def elemToRawContact(node: Node) : Option[RawContact] = {
    for(
      id <- (node \ "id").headOption;
      name <- (node \ "name").headOption;
      lastName <- (node \ "lastName").headOption;
      phone <- (node \ "phones").headOption
    ) yield RawContact(id.text, name.text, lastName.text, phone.text)
  }
}
