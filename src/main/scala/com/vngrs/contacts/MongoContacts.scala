package com.vngrs.contacts

import com.typesafe.config.{Config, ConfigFactory}
import reactivemongo.api.commands.{MultiBulkWriteResult, UpdateWriteResult}
import reactivemongo.api.indexes.{IndexType, Index}
import reactivemongo.api.{MongoConnection, BSONSerializationPack, MongoDriver}
import reactivemongo.api.collections.bson.{BSONBatchCommands, BSONCollection}
import reactivemongo.bson.{BSONArray, BSONDocument}
import reactivemongo.core.commands.RawCommand
import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.JavaConversions._

import scala.concurrent.Future
import scala.concurrent.duration._

/**
 * Created by husrevo on 25/10/15.
 */
class MongoContacts(config: Config = ConfigFactory.load().getConfig("mongodb"), mongoUri: Option[String] = None) {
  val server = config.getStringList("servers")
  val Array(dbName, collection) = Array("db", "collection").map(config.getString)
  val driver = new MongoDriver()

  val (mongoConn, db) =
    if(mongoUri.isDefined) {
      val parsedUri = MongoConnection.parseURI(mongoUri.get).get
      val connection = driver.connection(parsedUri)
      (connection, connection.db(parsedUri.db.getOrElse(dbName)))
    } else {
      val connection = driver.connection(server)
      (connection, connection.db(dbName))
    }
  val col = db.collection[BSONCollection](collection)

  def findByName(name: String, startsWith: Boolean, max: Int = Integer.MAX_VALUE) = {
    val query =
      if (startsWith)
        BSONDocument("name_clean" -> BSONDocument("$regex" -> ("^" + Filters.defaultFilter(name))))
      else
        BSONDocument("$text" -> BSONDocument("$search" -> name))
    col.find(query).cursor[MergedContact]().collect[List](max)
  }

  def saveToDB(contacts: List[MergedContact], wipe: Boolean = false): Future[UpdateWriteResult] = {
    val willThereBeAnyOtherRows =
      if (wipe) {
        col.drop().flatMap(_ => col.create()).map(_ => false)
      } else {
        col.count(limit = 1).map(_ > 0)
      }
    val result = willThereBeAnyOtherRows.flatMap { isThereAnyOtherRows =>
      if (isThereAnyOtherRows)
        upsertAll(contacts)
      else {
        col.indexesManager.create(Index(Seq(("name_clean", IndexType.Ascending))))
        col.indexesManager.create(Index(Seq(("name_search", IndexType.Text))))
        insertAll(contacts).map { r =>
          UpdateWriteResult(r.ok, r.n, r.nModified, Seq.empty, r.writeErrors, r.writeConcernError, r.code, r.errmsg)
        }
      }
    }
    result
  }

  private def insertAll(contacts: List[MergedContact]): Future[MultiBulkWriteResult] = {
    val bsons = contacts.map(implicitly[col.ImplicitlyDocumentProducer](_))
    col.bulkInsert(false)(bsons: _*)
  }

  /* ReactiveMongo doesn't have a straightforward way to bulk update, so we are using RAW command.
  *  Also, MongoDB supports upto 1k documents when updating in bulk.
  * */
  private def upsertAll(contacts: List[MergedContact]): Future[UpdateWriteResult] = {
    val futuresOfResult = contacts.grouped(1000).map { batch =>
      upsertBulkUpto1k(batch)
    }
    val futureOfResults = Future.sequence(futuresOfResult)
    val futureOfMergedResult = futureOfResults.map { results =>
      val zero = UpdateWriteResult(true, 0, 0, Seq.empty, Seq.empty, None, None, None)
      def mergeUpdateResults(uwr1: UpdateWriteResult, uwr2: UpdateWriteResult): UpdateWriteResult = {
        UpdateWriteResult(
          uwr1.ok || uwr2.ok,
          uwr1.n + uwr2.n,
          uwr1.nModified + uwr2.nModified,
          uwr1.upserted ++ uwr2.upserted,
          uwr1.writeErrors ++ uwr2.writeErrors,
          if (uwr1.writeConcernError.isEmpty) uwr2.writeConcernError else uwr1.writeConcernError,
          if (uwr1.code.isEmpty) uwr2.code else uwr1.code,
          if (uwr1.errmsg.isEmpty) uwr2.errmsg else uwr2.errmsg
        )
      }
      results.fold(zero) {
        case (uwr1, uwr2) =>
          mergeUpdateResults(uwr1, uwr2)
      }
    }
    futureOfMergedResult
  }

  private def upsertBulkUpto1k(contacts: List[MergedContact]): Future[UpdateWriteResult] = {
    def contactToUpsertBson(contact: MergedContact): BSONDocument = {
      BSONDocument(
        "q" -> BSONDocument("_id" -> contact.fullNameHash()),
        "u" -> BSONDocument(
          "$addToSet" -> BSONDocument(
            "phones" -> BSONDocument("$each" -> contact.phones),
            "ids" -> BSONDocument("$each" -> contact.ids)
          ),
          "$setOnInsert" -> contact
        ),
        "upsert" -> true
      )
    }
    val updates = contacts.map(contactToUpsertBson)
    val update = BSONDocument("update" -> collection, "updates" -> BSONArray(updates), "ordered" -> false)
    db.command(RawCommand(update)).map { r =>
      BSONSerializationPack.deserialize(r, BSONBatchCommands.UpdateReader)
    }
  }

  def close() = {
    mongoConn.close()
    driver.close(1 minute)
  }
}
