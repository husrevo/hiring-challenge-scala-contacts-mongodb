# README #

My code for a company's hiring challenge for Scala about Contacts.

The requirements stated that they want a cli app to bulk import some contacts from some given XML format, merge them by their full names and insert into MongoDB with a given format. I've added the ability merge too (to an already filled db)

I didn't have experience with MongoDB and ScalaTest, but still finished it in two days :) 

### Configuration ###
See src/main/resources/application.conf

### Tests ###

```
#!sh
./activator test
```

### Compile into JAR ###

```
#!sh
./activator assembly
```

Exports to target/scala_2.11/contacts.jar

### Running the JAR ###

```
#!sh
java -jar contacts.jar --help
```

tells it all :)

### TODO ###

I didn't have enough time to:

* Prepare a better README.md
* Solve the dirty shutdown of the Reactive Mongo driver (those Akka deadletter logs)
* Configure the log4j2 so it stops complaining.