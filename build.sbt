name := """contacts"""

version := "1.0"

scalaVersion := "2.11.7"

resolvers += Resolver.sonatypeRepo("public")

mainClass in assembly := Some("com.vngrs.contacts.MainCLI")

assemblyJarName in assembly := "contacts.jar"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.2.4" % "test",
  "org.scala-lang.modules" %% "scala-xml" % "1.0.5",
  "com.typesafe" % "config" % "1.3.0",
  "org.reactivemongo" %% "reactivemongo" % "0.11.7",
  "com.github.scopt" %% "scopt" % "3.3.0"
)



